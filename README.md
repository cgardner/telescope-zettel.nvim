# telescope-zettel.nvim

`telescope-zettel` is an extension for
[telescope.nvim](https://github.com/nvim-telescope/telescope.nvim) that
facilitates usage of a Zettelkasten. It allows you to search and open Zettels
from anywhere, paste Zettel links in various formats, etc.


## Installation
```lua
-- packer.nvim:
use { "https://gitlab.com/thlamb/telescope-zettel.nvim",
  requires = {
    {'nvim-telescope/telescope.nvim'},
    {'nvim-lua/plenary.nvim'},
  },
  config = function ()
    require"telescope".load_extension("zettel")
  end,
}
```

## Configuration
It is highly recommended to specify your Zettelkasten directory during the setup
of telescope. That way the extension will be able to reach your Zettel files
from any file you are currently working on. By default it will try to use the
current buffer's directory.

This extension also comes with various link style:
- Wiki style (`wiki`), _default_
- Regular markdown links (`md`)

And it can remove the extension when pasting links (`remove_ext`). _Note that
for markdown links, the extension will be removed only for the link name
  (between the square brackets), and not for the link itself (between the
  parentheses)._

```lua
require("telescope").setup {
  ...
  extensions = {
    zettel = {
      zk_path = "~/Documents/Zettel",
      link_style ="wiki" -- or "md"
      remove_ext = true -- or false
    }
  }
}
```

## Usage
`telescope-zettel` currently comes with two pickers: `find_zettel`,
`grep_zettels` which are equivalent to `find_file` and `live_grep` from
telescope, except that they only search through the Zettel folder mentioned in
`zk_path`.

Besides just finding zettels and opening them, the two pickers also come with
the same custom mappings that allow you to paste a link to a zettel file.


| Mapping | Link type                         | Wikistyle           | Markdown                    |
|---------| --------------------------------- | ------------------- |---------------------------- |
| `<C-r>` | Relative link to the current file | `[[../a/file]]`     | `[filename](../a/file)`     |
| `<C-a>`   | Absolute link to the zettel file  | `[[home/a/b/file]]` | `[filename](home/a/b/file)` |
| `<C-f>`   | Only filename                     | `[[filename]]`      | `[filename](filename)`      |


## Mappings
Users are required to specify the picker call mappings on their own if they want
one. Here are some examples

```lua
local map = require('config.utils').map
-- Mnemotechnic: Zettel Find, Zettel Grep
map('n', '<leader>zf', ':lua require("telescope").extensions.zettel.find_zettel()<CR>')
map('n', '<leader>zg', ':lua require("telescope").extensions.zettel.grep_zettels()<CR>')
```

Otherwise, the pickers can be called directly such as
```
:Telescope zettel find_zettel
```
