local has_telescope, telescope = pcall(require, "telescope")
local Path = require"plenary.path"

if not has_telescope then
  error("This plugin requires telescope.nvim (https://github.com/nvim-telescope/telescope.nvim)")
end

local M = {}
local zk_path = {}
local link_style = "wiki"
local remove_ext = true


 -- Find the relative path bewteen current file and chosen zettel
local function getRelPath(src,dst)

  -- find the common leading directories
  local idx = 0
  while (true) do
    local tst = src:find("/", idx + 1, true)
    if tst then
      if src:sub(1,tst) == dst:sub(1,tst) then
        idx = tst
      else
        break
      end
    else
      break
    end
  end

  -- if they have nothing in common return absolute path
  local first = src:find("/", 0, true)
  if idx <= first then
    return dst
  end

  -- trim off the common directories from the front
  src = src:sub(idx + 1)
  dst = dst:sub(idx + 1)

  -- back up from dst to get to this common parent
  local result = ""
  idx = src:find("/")
  while (idx) do
    result = result .. "../"
    idx = src:find("/", idx + 1)
  end
  result = result .. dst

  return result
end



local function pasteWikiLink(link)
  vim.api.nvim_paste('[['..link..']]', true, -1)
end

local function pasteMdLink(link,name)
  vim.api.nvim_paste('['..name..']'..'('..link..')', true, -1)
end

local function removeFileExt(file)
  return file:match("^(.+)%..+$")
end

local function trueFileName(file)
  return file:match("[^/]*.$")
end

local plugin_actions = {}

function plugin_actions.pasteLink(type)
  return function(prompt_bufnr)
    local entry = require("telescope.actions.state").get_selected_entry()
    require("telescope.actions").close(prompt_bufnr)

    local link = ""
    local thisfile = vim.fn.expand('%:p')

    if type == "abs" then
      link = entry.path
    elseif type == "rel" then
      link = getRelPath(thisfile,entry.path)
    elseif type == "file" then
      link = trueFileName(entry.filename)
    end


    -- Check if we can write on buffer
    if vim.api.nvim_buf_get_option(vim.api.nvim_get_current_buf(), "modifiable") then
      -- Paste link
      if link_style == "md" then
        local linkname = trueFileName(entry.filename)
        if remove_ext then
          linkname = removeFileExt(linkname)
        end
        pasteMdLink(link,linkname)
      else
        if remove_ext then
          pasteWikiLink(removeFileExt(link))
        else
          pasteWikiLink(link)
        end
      end
    end

  end
end




M.find_zettel = function(opts)
  opts = opts or {}
  opts.prompt_title = "Zettel - Files"
  opts.cwd = zk_path
  opts.attach_mappings = function(_, map)
    map("i", "<c-r>", plugin_actions.pasteLink("rel") )
    map("i", "<c-a>", plugin_actions.pasteLink("abs") )
    map("i", "<c-f>", plugin_actions.pasteLink("file") )
    map("n", "<c-r>", plugin_actions.pasteLink("rel") )
    map("n", "<c-a>", plugin_actions.pasteLink("abs") )
    map("n", "<c-f>", plugin_actions.pasteLink("file") )
    return true
  end
  require("telescope.builtin").find_files(opts)
end


M.grep_zettels = function(opts)
  opts = opts or {}
  opts.cwd = zk_path
  opts.prompt_title =  "Zettel - grep in files"
  opts.attach_mappings = function(_, map)
    map("i", "<c-r>", plugin_actions.pasteLink("rel") )
    map("i", "<c-a>", plugin_actions.pasteLink("abs") )
    map("i", "<c-f>", plugin_actions.pasteLink("file") )
    map("n", "<c-r>", plugin_actions.pasteLink("rel") )
    map("n", "<c-a>", plugin_actions.pasteLink("abs") )
    map("n", "<c-f>", plugin_actions.pasteLink("file") )
    return true
  end
  require("telescope.builtin").live_grep(opts)
end

return telescope.register_extension {
  setup = function(ext_config)
    zk_path = Path:new(ext_config.zk_path):normalize()  or {}
    link_style = ext_config.link_style  or link_style
    remove_ext = ext_config.remove_ext
    if remove_ext == nil then remove_ext = true end
  end,
  exports = {
    find_zettel = M.find_zettel,
    grep_zettels = M.grep_zettels,
  },
}
